import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,  ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule , routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';  
// import {CommonModule } from '@angular/Common';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule, NgbDateAdapter, NgbDateNativeAdapter, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap'; 
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {LayoutModule} from '@angular/cdk/layout';  
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { FileUploaderModule } from "ng4-file-upload";
import { ChartModule } from "primeng/primeng";


import {MatToolbarModule, 
  MatButtonModule,
  MatCheckboxModule,
  MatSidenavModule, 
  MatIconModule, 
  MatListModule,  
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonToggleModule,
  MatCardModule,
  MatChipsModule,
  //MatDatepickerModule, 
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatInputModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatTooltipModule,
  MatTreeModule,
  
 } from '@angular/material';
import { ContactComponent } from './contact/contact.component';
import { RegistrationComponent } from './registration/registration.component';
import { CompanyJobsComponent } from './company-jobs/company-jobs.component';
import { CareersComponent } from './careers/careers.component';
import { JobsComponent } from './jobs/jobs.component';
import { ApplyNowComponent } from './apply-now/apply-now.component';
import { MatcherReportComponent } from './matcher-report/matcher-report.component';
import { ScaleComponent } from './scale/scale.component';
import { MatcherChartsComponent } from './matcher-charts/matcher-charts.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { NgbDateFRParserFormatter,  } from './utils/date.adapter';
@NgModule({
  declarations: [
    AppComponent, 
    routingComponents,
    HeaderComponent,
    FooterComponent,
    ContactComponent,
    RegistrationComponent,
    CompanyJobsComponent,
    CareersComponent,
    JobsComponent,
    ApplyNowComponent,
    MatcherReportComponent,
    ScaleComponent,
    MatcherChartsComponent 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule, 
    FormsModule,
    ReactiveFormsModule,
    // CommonModule,
    HttpClientModule ,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule, 
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    FileUploaderModule,
    ChartModule,
    NgbModule
  ],
  providers: [{provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter}],
  bootstrap: [AppComponent]
})
export class AppModule { }

 