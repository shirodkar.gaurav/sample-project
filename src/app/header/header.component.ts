import { Component,Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() headerData:any;
  homelink:boolean = false;
  aboutuslink:boolean = false;
  careerlink:boolean = false;
  joblink:boolean = false;
  companylink:boolean = false;  
  contactlink:boolean = false;
  constructor() { 
    // this.showHeader = false;
  }

  ngOnInit() {
    this.homelink = this.headerData['homelink'];
    this.aboutuslink = this.headerData['aboutuslink'];
    this.careerlink = this.headerData['careerlink'];
    this.joblink = this.headerData['joblink'];
    this.companylink = this.headerData['companylink'];
    this.contactlink = this.headerData['contactlink'];
  }
}
