import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-apply-now',
  templateUrl: './apply-now.component.html',
  styleUrls: ['./apply-now.component.scss']
})
export class ApplyNowComponent implements OnInit {

  resumeOption = 1;
  url;

  @Input()
  jobDetails;
  
  constructor() { }
  @Output()
  applyForJob  = new EventEmitter();
  ngOnInit() {
  }

  fileuploaderFileChange(files: FileList){

  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event:any) => { // called once readAsDataURL is completed
        this.url = event.target.result;
      }
    }
  }
  applyForJobfn(){
    this.applyForJob.emit('');
  }
}
