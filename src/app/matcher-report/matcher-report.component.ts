import { Component, OnInit, ViewChild, Output, EventEmitter, ViewChildren } from '@angular/core';


@Component({
  selector: 'app-matcher-report',
  templateUrl: './matcher-report.component.html',
  styleUrls: ['./matcher-report.component.scss']
})
export class MatcherReportComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  matcherReportdetailsOption = -1;
   
  skillsAsked=[
    {skill:"Machine Learning",color:"#bde4ff"},
    {skill:"Python",color:"#bde4ff"},
    {skill:"Deep Learning",color:"#336688"},
    {skill:"Model Delivery",color:"#336688"},
    {skill:"Pytorch",color:"#336688"},
    
  ];
  
  yourSkills=[
    {skill:"Machine Learning", color:"#bde4ff"},
    {skill:"Python", color:"#bde4ff"},
    {skill:"Neural Network", color:"#336688"},
    {skill:"Team Leadership", color:"#336688"},
    {skill:"Keras", color:"#336688"}
  ];
  yourSkills1=this.yourSkills
  // yourSkills2=this.yourSkills


  //
  previousRoles=[
    {skill:"Deep Learning", color:"#336688"},
    {skill:"R", color:"#336688"},
    {skill:"SAS", color:"#336688"},
    {skill:"Data Cleaning", color:"#bbb"},
    {skill:"Data Acquisition", color:"#bbb"},
  ];
  previousRoles1 = this.previousRoles;
  //
  dateSelected= new Date()
  fromDate1 = new Date();

  fromdate1={
    "year": 2017,
    "month": 5,
    "day": 1
  };
  fromdate2={
    "year": 2016,
    "month": 9,
    "day": 1
  };
  todate2={
    "year": 2017,
    "month": 4,
    "day": 1
  };
  wrongtext1="- Independently deliver new machine learning and neural network models via my team of data scientists\
   \n- Collaborate with various business stakeholders in the Compliance and Audit teams to design solutions from data analytics as needed for their specific projects \
   \n- Hands on modelling experience with tools including Tensorflow and Keras \
   \n- Design data acquisition and pre-processing pipelines using Python";
   wrongtext2="- Assisted the data science team in data acquisition and cleaning tasks (mostly using R and SAS pipelines)\
   \n- Built deep learning models that were well appreciated by the Data Science team";

@Output()
applyForJob = new EventEmitter();

 onDateChange(event) {
    
      // this.onSelection.emit(this.dateSelected);
    
  }

  fromDate1Changed(){
    let dt:Date = this.fromDate1
    // this.fromDate1 = dt.getDate() + '/' + dt.getFullYear()
  }
   filterChangefn(){
    if(this.matcherReportdetailsOption == 2){
      // this.yourSkills1 = this.yourSkills
      // this.previousRoles1 = this.previousRoles
      this.yourSkills1 = this.yourSkills.filter(el =>{return el.color == "#336688" || el.color == "#bde4ff"})
      this.previousRoles1 = this.previousRoles.filter(el => {return el.color == "#336688" || el.color == "#bde4ff"})
    }else{
      this.yourSkills1 = this.yourSkills.filter(el =>{return el.color == "#bde4ff"})
      this.previousRoles1 = this.previousRoles.filter(el => {return el.color == "#bde4ff"})
    }
  }
  matcherReportclearFilter(){
    this.matcherReportdetailsOption = -1;
     this.yourSkills1 = this.yourSkills
      this.previousRoles1 = this.previousRoles
  }
  applyForTheJob(){
    this.applyForJob.emit('')
  }
}
