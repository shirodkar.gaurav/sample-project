import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatcherReportComponent } from './matcher-report.component';

describe('MatcherReportComponent', () => {
  let component: MatcherReportComponent;
  let fixture: ComponentFixture<MatcherReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatcherReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatcherReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
