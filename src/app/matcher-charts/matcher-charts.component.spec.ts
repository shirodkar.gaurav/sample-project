import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatcherChartsComponent } from './matcher-charts.component';

describe('MatcherChartsComponent', () => {
  let component: MatcherChartsComponent;
  let fixture: ComponentFixture<MatcherChartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatcherChartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatcherChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
