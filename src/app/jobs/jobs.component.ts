import { Component, OnInit } from '@angular/core';
export interface distance {
  value: string;
  viewValue: string;
}
export interface city {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss']
})
export class JobsComponent implements OnInit {
  homeHeaderData = { 'homelink': true, 'aboutuslink': true, 'careerlink': true, 'joblink': true, 'companylink': true, 'contactlink': true };
  selectedValue: string;
  selectedValuedistance: string;
  selectedValuecountry: string;
  selectedValuestate: string;
  selectedValuecity:string;
  applied:boolean = false;
  distances: distance[] = [
    { value: '5Miles-0', viewValue: '5Miles' },
    { value: '10Miles-1', viewValue: '10Miles' },
    { value: '15Miles-2', viewValue: '15Miles' },
    { value: '20Miles-2', viewValue: '20Miles' },
    { value: '25Miles-2', viewValue: '25Miles' }
  ];
  cities: city[] = [
    { value: 'sfba', viewValue: 'SF Bay Area' },
    { value: 'nyc', viewValue: 'Greater New York City' },
    { value: 'atlanta', viewValue: 'Atlanta Los Angeles' },
    { value: 'denver', viewValue: 'Denver' }
  ];
skillsAsked=[
  {skill:"Machine Learning",color:"#bde4ff"},
  {skill:"Python",color:"#bde4ff"},
  {skill:"Deep Learning",color:"#336688"},
  {skill:"Model Delivery",color:"#336688"},
  {skill:"Pytorch",color:"#336688"},
  
];

yourSkills=[
  {skill:"Machine Learning", color:"#bde4ff"},
  {skill:"Python", color:"#bde4ff"},
  {skill:"Neural Network", color:"#336688"},
  {skill:"Team Leadership", color:"#336688"},
  {skill:"Keras", color:"#336688"}
];
yourSkills1=this.yourSkills
previousRoles=[
  {skill:"Deep Learning", color:"#bde4ff"},
  {skill:"R", color:"#336688"},
  {skill:"SAS", color:"#336688"},
  {skill:"Data Cleaning", color:"#bbb"},
  {skill:"Data Acquisition", color:"#bbb"},
];
previousRoles1 = this.previousRoles;
  data = [];
  data2 = [
    { id: 1, role: "Data Scientist", jobid: "2019-DS1", datetime: "19 Mar 2019 | 05:35 pm", jobindustry: 'Financial Service & Data Science', companyname: "AssetMark Inc.", joblocation: "Concord CA", detailpera: "The Data Science team is a core competency of IT that enables AssetMark Financial to deliver a platform for independent financial advisors to build great businesses. At the heart of this platform is a Model Ecosystem. The Model Ecosystem acts as the firm’s single location for all data science, analytical reporting, dashboards and KPIs. The Data Scientist will act as part of Data Science team that is responsible for the design, development and management of the company’s predictive, explanatory and other models. The successful candidate will be part of a team that ingests data, models it, and disseminates insights throughout the organization line of business systems, strategic business partners and management." ,
    principalResponsibilities:[
      "Responsible for end to end delivery of models to be utilized for scorecards, dashboards and other insights on marketing, distribution, advisor, financial and other data",
      "Manage a small team of associate data scientists aligned to a specific LOB",
      "Understand business requirements - translate and use complex data sources that needs to be blended, cleaned, structured and enriched into a desired output for analysis",
      "Comfortable to work collaboratively with multiple application development teams",
      "Big Picture thinker with the ability to execute with details",
      "Serve as the technical subject-matter expert for the Model Ecosystem for the assigned technology scrum teams."
    ],
    requiredQualifications:[
      "Minimum 3 years working as a Data Scientist",
      "Hands on experience with various machine learning concepts and practices",
      "Experience with Deep Learning frameworks - preferred Pytorch",
      "Hands on coding experience with Python",

      
    ],
    preferredQualifications:[],
    requiredSkills:[],
    aboutCompany:"AssetMark, Inc. is a leading strategic provider of groundbreaking investment and consulting solutions created to serve independent financial advisors and help them create great outcomes for their clients. Through a consistent program of engagement, education and community-building, AssetMark brings like-minded advisors together to share their ideas, experiences and solutions. This collaboration guides our development of investment, client relationship and practice management tools that advisors can tap to help clients achieve their investment objectives and life goals. We are always seeking bright, creative, energetic individuals who want to learn and grow with the company. AssetMark is an innovative organization, delivering the best services in the industry. We offer a rewarding work environment, competitive salaries, full benefits, and excellent opportunities for career growth. We are headquartered in the San Francisco Bay Area. We also have regional offices in Phoenix, AZ, Encino, CA, Atlanta, GA, Chicago, IL, and State College, PA. Become a part of our growing team today!"
    
  },
    { id: 2, role: "Senior Software Engineer", jobid: "xx-xxx", datetime: "19 Mar 2019 | 05:35 pm", jobindustry: 'IT industry', companyname: "AssetMark", joblocation: "Concord, CA", detailpera: "The IT Tech Lead will research, design, develop, configure, integrate, test and maintain existing and new trading applications and/or integrate 3rd party developed applications with Asset Mark’s systems. Responsible for conducting, and coordinating software development activities throughout the project, including key design decisions. Candidate will work closely with Application Architects and business SMEs and will be reporting to the IT Manager of Back Office Systems and Data.",
              principalResponsibilities:[
                "Document key SDLC processes by producing formal documents like Solution Architecture Document Technical System Design Specifications, etc.",
                "Software analysis, code analysis, requirements analysis, software review, identification of code metrics, system risk analysis, software reliability analysis",
                "Collaborate with 3rd party vendors and develop suitable integration solutions",
                "Conduct formal and informal training and learning sessions with Team members.",
                "Provides accurate estimates (Resource / Time) to Management for work activities",
                "Applies in-depth technical knowledge to provide maintenance solutions across one or more technology areas (e.g. trading and rebalancing).",
                "Participates in troubleshooting complex issues and resolving defects",
                "Provide technical thought leadership and be a strong collaborator with domain SMEs, application architects and senior development teams and management",
                "Adopt new and emerging technologies to provide solutions to client needs",
                "Perform Code Review sessions with the development team to improve the quality of the system / Processes developed.",
                "Developing new user-facing features using React.js",
                "Help in development of Web APIs using .NET Core",
                "Building reusable components and front-end libraries for future use Knowledge, Skills and Abilities:",
                "BS/BA in Computer Science, Engineering, Information Systems and/or equivalent formal training or experience",
                "10+ years of experience in Information Technology supporting development of complex projects involving significant exposure to trading, trading review and risk systems",
                "Expertise in Design and Development of Web / Windows applications in various Microsoft technologies such as (C#, VB.Net",
                "5+ years of experience with a broker dealer or wealth management company is preferred",
                "Deep understanding of SQL Server 2008/2012 and the appropriate and effective use of database objects",
                "Familiarity with Agile development methodologies (SAFE",
                "Familiarity with Source Code management systems such as TFS and GIT.",
                "Familiarity with project management systems such as TFS/JIRA / MS Project / Excel",
                "Broad understanding of the trading technologies including order management systems, FIX and their inter-operability",
                "Ability to quickly grasp new technology concepts",
                "Object-oriented Design and Analysis (OOA and OOD) concepts",
                "Excellent communication skill",
                "Good knowledge and experience with other vendor’s platforms, products, companies and philosophies",
                "Demonstrated expertise in collaborating in a matrixed architecture team delivering timely results.",
                "Strong proficiency in JavaScript, including DOM manipulation and the JavaScript object model",
                "Strong Proficiency in .NET Core with reference to Web API development",
                "Thorough understanding of React.js and its core principles",
                "Experience with popular React.js workflows and templating engines (Examples: Flux or Redux)"
              ],
              requiredQualifications:[],
              preferredQualifications:[],
              requiredSkills:[],
              aboutCompany:"AssetMark, Inc. is a leading strategic provider of groundbreaking investment and consulting solutions created to serve independent financial advisors and help them create great outcomes for their clients. Through a consistent program of engagement, education and community-building, AssetMark brings like-minded advisors together to share their ideas, experiences and solutions. This collaboration guides our development of investment, client relationship and practice management tools that advisors can tap to help clients achieve their investment objectives and life goals.We are always seeking bright, creative, energetic individuals who want to learn and grow with the company. AssetMark is an innovative organization, delivering the best services in the industry. We offer a rewarding work environment, competitive salaries, full benefits, and excellent opportunities for career growth. We are headquartered in the San Francisco Bay Area. We also have regional offices in Phoenix, AZ, Encino, CA, Atlanta, GA, Chicago, IL, and State College, PA. Become a part of our growing team today!"
            },

    { id: 3, role: "Senior Data Analyst", jobid: "xx-xxx", datetime: "19 Mar 2019 | 05:35 pm", jobindustry: 'IT industry', companyname: "AssetMark", joblocation: "Delaware, DE", detailpera: "The Data Services team is a core competency of IT that enables AssetMark Financial to deliver a platform for independent financial advisors to build great businesses. At the heart of this platform is an Enterprise Data Hub. The EDH acts as the firm’s single source of truth for data and is the foundation for all data and analytical reporting, dashboards and KPIs.The Senior Data Analyst will act as part of Data Services Platform team that is responsible for the design, development and management of the company’s database and data warehouse platforms. The successful candidate will be part of a team that ingests, models, prepares, stores and disseminates data throughout the organization line of business systems, strategic business partners and management." ,
    principalResponsibilities:[
      "Responsible for designing, developing and deploying of data preparation processes to be utilized for data warehouses/repositories/big data platforms, self-service reporting solutions, scorecards and dashboards on financial data using analytical and visualization tools.",
      "Understand business requirements - translate and use complex data sources that needs to be blended, cleaned, structured and enriched into a desired output for analysis and utilized to deliver efficient and quality reports/ dashboards using various BI tools",
      "Comfortable to work collaboratively with multiple application development teams.",
      "Big Picture thinker with the ability to execute with details. Serve as the technical subject-matter expert for the assigned scrum teams.",
      "Promote and evaluate use of software development best practices (including automated unit testing, Continuous Integration, Continuous Deployment, Dev Ops)",
      "Work in an object-oriented, multi-developer environment with version control, change management and continuous improvement protocols.",
    ],
    requiredQualifications:[
      "Minimum 3 years working as a Senior Data Analyst",
      "2+ years of experience with Big Data concepts and common components including Azure, Spark and multiple languages (Java, Scala, Python, R) and integration with consumption tools.",
      "Advanced SQL, PowerShell and/or Python scripting experience.",
      "Strong technology background with 5+ years of ETL and SQL coding experience",
      "3+ years of .Net (C#, VB Script) OR Java application development experience",
      "3+ years of ETL coding experience using SSIS, Azure Data Factory and other tools",
      "Experience in trouble shooting and resolving issues with existing systems",
      "Experience designing high performing database queries / indexes and troubleshooting database performance issues.",
      "Excellent written and verbal communication skills.",
      "Ability to deliver in a fast-paced and goals-based environment with time-bound deliverables",
      "Ability to work flexible hours to meet deadlines",
      "Skilled in collaboration, enjoys working with others and achieving results as a team",
      "Passion and demonstrated experience in delivering results and meeting business goals in a matrix and cross functional environment",
      "Work with Onshore and Offshore vendor teams including development and QA resources",
      "MS Office (Word, Excel, Visio, PowerPoint)"
    ],
    preferredQualifications:[
      "Strong hands-on experience in Paxata or other data profiling technologies",
      "Undergraduate degree in Information Systems, Computer Science, or Data Science",
      "1+ years of Spark development",
      "Azure Databricks experience",
      "Experience in analysis of financial services data strongly preferred",
      "Facilitation/presentation experience and ability to properly communicate with Business and Technical audience"
    ],
    requiredSkills:[],
    aboutCompany:"AssetMark, Inc. is a leading strategic provider of groundbreaking investment and consulting solutions created to serve independent financial advisors and help them create great outcomes for their clients. Through a consistent program of engagement, education and community-building, AssetMark brings like-minded advisors together to share their ideas, experiences and solutions. This collaboration guides our development of investment, client relationship and practice management tools that advisors can tap to help clients achieve their investment objectives and life goals.We are always seeking bright, creative, energetic individuals who want to learn and grow with the company. AssetMark is an innovative organization, delivering the best services in the industry. We offer a rewarding work environment, competitive salaries, full benefits, and excellent opportunities for career growth. We are headquartered in the San Francisco Bay Area. We also have regional offices in Phoenix, AZ, Encino, CA, Atlanta, GA, Chicago, IL, and State College, PA. Become a part of our growing team today!"
    },

    { id: 4, role: "Lead Data Governance Analyst", jobid: "xx-xxx", datetime: "19 Mar 2019 | 05:35 pm", jobindustry: 'IT industry', companyname: "AssetMark", joblocation: "Chicago, IL", detailpera: "Data governance is a core competency that enables AssetMark Financial to deliver a platform for independent financial advisors to build great businesses. At the heart of this platform is a web enterprise application that uses a combination of best-in-class industry standard applications integrated with proprietary applications using the .NET framework.This Lead Data Governance Analyst will act as part of Data Service Governance team that is responsible for ensuring our Enterprise Data Hub can be the company’s single source of truth.  The successful candidate will be able to raise data quality and performance levels of our platform, provide a high level of data quality awareness across the organization, proactively improve the quality of corporate reporting, and evaluate and identify system enhancement are required.The Lead Data Governance Analyst will also conduct detailed research and work with IT and business subject matter experts to gather knowledge and assist with the creation of enterprise application strategy, policies, and standards." ,
    principalResponsibilities:[
      "Collaborate with IT, Sales, Marketing and Operations SMEs to define, document, and execute data quality fixes, enhancements, projects, and new capabilities",
      "Ensure adherence to department data governance standards",
      "Maintain integrity of project documentation throughout all project life cycles",
      "Proactively investigate options to solve business problems",
      "Create domain models, workflows, process flows, data flows, and interaction models",
      "Prepare test strategies, test plans, and test cases",
      "Assess emerging technologies and recommend integration points into the data governance framework"
    ],
    requiredQualifications:[
      "4 + years of experience working in a data analyst role",
      "2+ years’ experience in data mining",
      "4-year college degree in relevant field, or equivalent work experience.",
      "Strong analytical, data mining, data modeling and data management skills",
      "Data profiling experience with tools such as Paxata, Informatica, PowerBI and/or Talend",
      "Data discovery",
      "Information chain analysis and management",
      "Root cause analysis",
      "Excellent written and verbal communication skills.",
      "Ability to deliver in a fast-paced and goals-based environment with time-bound deliverable",
      "Ability to work flexible hours to meet deadlines",
      "Skilled in collaboration, enjoys working with others and achieving results as a team",
      "Passion and demonstrated experience in delivering results and meeting business goals in a matrix and cross functional environment",
      "Work with Onshore and Offshore vendor teams including development and QA resources",
      "Requirements gathering",
      "MS Office (Word, Excel, Visio, PowerPoint)"
    ],
    preferredQualifications:[
      "Experience with Paxata",
      "Experience with a variety of industry standards regarding Microsoft database development concepts, best practices, and procedures",
      "Experience with financial services industry is a plus"
    ],
    requiredSkills:[],
    aboutCompany:"AssetMark, Inc. is a leading strategic provider of groundbreaking investment and consulting solutions created to serve independent financial advisors and help them create great outcomes for their clients. Through a consistent program of engagement, education and community-building, AssetMark brings like-minded advisors together to share their ideas, experiences and solutions. This collaboration guides our development of investment, client relationship and practice management tools that advisors can tap to help clients achieve their investment objectives and life goals.We are always seeking bright, creative, energetic individuals who want to learn and grow with the company. AssetMark is an innovative organization, delivering the best services in the industry. We offer a rewarding work environment, competitive salaries, full benefits, and excellent opportunities for career growth. We are headquartered in the San Francisco Bay Area. We also have regional offices in Phoenix, AZ, Encino, CA, Atlanta, GA, Chicago, IL, and State College, PA. Become a part of our growing team today!"
    },

    { id: 5, role: "Senior Software Engineer", jobid: "xx-xxx", datetime: "19 Mar 2019 | 05:35 pm", jobindustry: 'IT industry', companyname: "AssetMark", joblocation: "Concord, CA", detailpera: "AssetMark is a leading strategic provider of innovative investment and consulting solutions serving independent financial advisors. We provide investment, relationship and practice management solutions that advisors use in helping clients achieve wealth, independence and purpose.We are looking for an experienced senior software engineer who is passionate about building amazing enterprise services with cutting edge technologies. This is an exciting opportunity to transform the culture in a business that is ready to disrupt the industry and excited about how cutting edge technology can enable their business success. You will be joining the Applications Development Solutions Team focused on delivering strategic projects. You will have the opportunity and broad purview to improve team efficiency and productivity by setting high quality engineering standards." ,
    principalResponsibilities:[
      "Experience in architecting, designing, testing and implementing large-scale, high volume transactional and highly available systems in the Retail, Web or Financial Services Industry",
      "Build and maintain software products and plan the best solution, keeping both the backend and UI in mind.",
      "Take full ownership of a product from conception through to production.",
      "Turn requirements into simple, elegant, optimal solutions",
      "Operate the software you build in a DevOps model",
      "Architect and build server side platforms using Microsoft technologies",
      "Establish development best practices for technical documentation, code management and proper branching and release strategy.",
      "Participates in trouble-shooting complex issues and resolving defects"
    ],
    requiredQualifications:[
      "Minimum of 10 years’ progressive technology experience required",
      "Deep understanding of good software design patterns mixed with a healthy passion for refactoring and keeping things simple",
      "Must have excellent interpersonal relationship and influencing skills",
      "Experience with automation/configuration management using advanced MS technologies",
      "Expert-level expertise with Web Service Development (RESTful/SOAP) and micro-services architecture on Microsoft .NET Core platform",
      "Strong experience with web technologies and responsive design and use of HTML5, CSS3, jQuery, Angular and/or React-JS, Bootstrap, XML, JSON",
      "Strong knowledge of Database technologies preferably SQL server and/or Oracle. Ability to design the data model and Entity Relationship of database objects. Familiarity with Microsoft Entity Framework",
      "Expert level familiarity with SAML2 and OAuth identity management and identity federation services",
      "Understanding of web application security best practices, OWASP and designing secure applications",
      "Experience with fundamentals of agile culture, projects and team",
      "Ability to deliver in a fast-paced and goals-based environment with time-bound deliverables ",
      "Exceptional technical skills, marked by poise, positive influence, and the ability to drive change",
      "Strong analytical and strategic thinking skills, with the ability to influence senior product leaders across both the business and technology",
      "Strong interpersonal, presentation, and communications skills (written and oral)",
      "Attention to detail, good team dynamics, and an ability to work independently as needed"
    ],
    preferredQualifications:[],
    requiredSkills:[],
    aboutCompany:"AssetMark, Inc. is a leading strategic provider of groundbreaking investment and consulting solutions created to serve independent financial advisors and help them create great outcomes for their clients. Through a consistent program of engagement, education and community-building, AssetMark brings like-minded advisors together to share their ideas, experiences and solutions. This collaboration guides our development of investment, client relationship and practice management tools that advisors can tap to help clients achieve their investment objectives and life goals.We are always seeking bright, creative, energetic individuals who want to learn and grow with the company. AssetMark is an innovative organization, delivering the best services in the industry. We offer a rewarding work environment, competitive salaries, full benefits, and excellent opportunities for career growth. We are headquartered in the San Francisco Bay Area. We also have regional offices in Phoenix, AZ, Encino, CA, Atlanta, GA, Chicago, IL, and State College, PA. Become a part of our growing team today!"
    },


    { id: 6, role: "Systems Administrator II", jobid: "xx-xxx", datetime: "19 Mar 2019 | 05:35 pm", jobindustry: 'IT industry', companyname: "AssetMark", joblocation: "Concord, CA", detailpera: "he Systems Administrator II is a key member of the Production team that supports the applications and infrastructure used internally by AssetMark employees and externally by financial advisors. This position participates in the application development process by creating and maintaining development environments and providing feedback on the production impact of projects under development.The associate in this role works within the framework of established operating procedures and in close cooperation with internal and external support personnel to provide troubleshooting and maintenance of applications, networks, operating systems and servers to assure operational availability. They will identify, implement, and document new procedures as appropriate, when existing procedures are obsolete or new technologies are introduced to the environment. Whenever possible and feasible, they strive for solid automated processes for management of the configuration and deployment of the production environment. Their focus is on both application and hardware aspects production systems including application services, database services, .NET framework, and AssetMark application-specific configuration and interaction requirements. This position acts as point of escalation and second line support for application support technicians, DBAs, QA associates and Program Managers since they understand implication of implementation details across the AssetMark application environment.This role interacts with networking technologists, application support, DBAs, helpdesk, and other system administrators to assist in production environment troubleshooting and issue resolution, as well as project managers, business analysts, developers, and architects within and outside of the IT department to ensure the successful implementation and deployment of AssetMark features, programs, and automated business processes." ,
    principalResponsibilities:[],
    requiredQualifications:[],
    preferredQualifications:[],
    requiredSkills:[
      "Establish system specifications and standards using industry best practices;",
      "Design, implementation and support of IIS 7.5 webservers for high performance and high availability;",
      "Experience in automated configuration and administration of enterprise systems and applications;",
      "Excellent troubleshooting skills using native Performance Monitor, Sysinternals tools and packet capture analysis;",
      "Infrastructure and application monitoring systems and how to make best use of them in a production environment;",
      "Windows Server 2016 setup and administration, including NTFS permissions, Active Directory, Group Policies, MSCS clustering and server imaging; ",
      "Experience designing, implementing, and managing both onsite and hosted email solutions;",
      "Knowledge of the following networking components: Switches, Firewalls, Load Balancers, IDS/IPS devices and Proxy Servers;"
    ],
    requiredExperience: [
      "Developing and executing IT policies and procedures;",
      "Experience passing regulatory compliance audits such as SOX;",
      "2+ years experience with internal/external security audits;",
      "Working primarily in a Microsoft environment using .NET technologies;",
      "Experience within a 24x7 IT operation with 3- to 5-nines application and system uptime while participating in a hotline rotation;",
      "Keen eye on the clock awareness for production availability while troubleshooting and providing root cause analysis;",
      "Excellent written and verbal communication skills;",
      "Ability to manage projects and direct others in implementing technical tasks;",
      "Ability to train others in technical areas and provide leadership by example;",
      "Ability to support technical needs of software developers;",
      "Interest and willingness to learn about the AssetMark business and participate in business planning as relates to the technology platform and options for better supporting AssetMark’s strategy;",
      "Interest and willingness to learn about the AssetMark business and participate in business planning as relates to the technology platform and options for better supporting AssetMark’s strategy;",
      "General knowledge of SQL server 2008/2012/2016 and the SQL query language;",
      "Using TFS or similar source control for ticketing, general programming and code interpretation for understanding;",
      "Knowledge of VMware vSphere and Microsoft Hyper-V virtualization technologies;",
      "General SAN administration, including cloning and snapshotting for high availability and other uses;",
      "Virus Scanning Administration;",
      "Experience with Docker containers;",
      "Centralized Patch Administration (OS and Firmware);",
      "Data backup and replication;",
      "Experience with Azure Cloud services;",
      "Education and Employment Experience:",
      "B.S. in Computer Science, or equivalent",
      "10+ years in the IT field",
      "Experience in SaaS or Hosted Systems is highly desirable",
      "Managing systems containing PII (personally identifiable information) a plus"
    ],
    aboutCompany:"AssetMark, Inc. is a leading strategic provider of groundbreaking investment and consulting solutions created to serve independent financial advisors and help them create great outcomes for their clients. Through a consistent program of engagement, education and community-building, AssetMark brings like-minded advisors together to share their ideas, experiences and solutions. This collaboration guides our development of investment, client relationship and practice management tools that advisors can tap to help clients achieve their investment objectives and life goals.We are always seeking bright, creative, energetic individuals who want to learn and grow with the company. AssetMark is an innovative organization, delivering the best services in the industry. We offer a rewarding work environment, competitive salaries, full benefits, and excellent opportunities for career growth. We are headquartered in the San Francisco Bay Area. We also have regional offices in Phoenix, AZ, Encino, CA, Atlanta, GA, Chicago, IL, and State College, PA. Become a part of our growing team today!"
    },

    { id: 7, role: "Senior Software Engineer (Technical Lead)", jobid: "xx-xxx", datetime: "19 Mar 2019 | 05:35 pm", jobindustry: 'IT industry', companyname: "AssetMark", joblocation: "Atlanta, GA", detailpera: "The Tech Lead (Senior Software Engineer) will research, design, develop, configure, integrate, test and maintain existing and new trading applications and/or integrate 3rd party developed applications with Asset Mark’s systems. Responsible for conducting, and coordinating software development activities throughout the project, including key design decisions. Candidate will work closely with Application Architects and business SMEs and will be reporting to the IT Manager of Back Office Systems and Data." ,
    principalResponsibilities:[
      "Design, Code, debug and Develop batch oriented file processing & Trading application development.",
      "Document key SDLC processes by producing formal documents like Solution Architecture Document Technical System Design Specifications, etc.",
      "Software analysis, code analysis, requirements analysis, software review, identification of code metrics, system risk analysis, software reliability analysis ",
      "Collaborate with 3rd party vendors and develop suitable integration solutions",
      "Conduct formal and informal training and learning sessions with Team members. ",
      "Provides accurate estimates (Resource / Time) to Management for work activities ",
      "Applies in-depth technical knowledge to provide maintenance solutions across one or more technology areas (e.g. trading and rebalancing).",
      "Participates in troubleshooting complex issues and resolving defects ",
      "Provide technical thought leadership and be a strong collaborator with domain SMEs, application architects and senior development teams and management",
      "Adopt new and emerging technologies to provide solutions to client needs",
      "Perform Code Review sessions with the development team to improve the quality of the system / Processes developed. ",

    ],
    requiredQualifications:[],
    preferredQualifications:[],
    requiredSkills:[
      "BS/BA in Computer Science, Engineering, Information Systems and/or equivalent formal training or experience ",
      "10+ years of experience in Information Technology supporting development of complex projects involving significant exposure to trading, trading review and risk systems",
      "Expertise in Design and Development of Web / Windows applications in various Microsoft technologies such as (C#, VB.Net)",
      "Advanced level of expertise with understanding custodian data and trading interfaces",
      "5+ years of experience with a broker dealer or wealth management company is preferred",
      "Advanced level of experience with managing trade data, allocations and / or trade review systems",
      "Strong experience with SQL Server (DB Design, Query Optimization, Indexing)",
      "Deep understanding of SQL Server 2008/2012 and the appropriate and effective use of database objects",
      "Familiarity with Agile development methodologies (SAFE)",
      "Familiarity with Source Code management systems such as TFS and GIT. ",
      "Familiarity with project management systems such as TFS/JIRA / MS Project / Excel. ",
      "Broad understanding of the trading technologies including order management systems, FIX and their inter-operability ",
      "Ability to quickly grasp new technology concepts. ",
      "Object-oriented Design and Analysis (OOA and OOD) concepts. ",
      "Excellent communication skills",
      "Good knowledge and experience with other vendor’s platforms, products, companies and philosophies. ",
      "Demonstrated expertise in collaborating in a matrixed architecture team delivering timely results. "
    ],
    aboutCompany:"AssetMark, Inc. is a leading strategic provider of groundbreaking investment and consulting solutions created to serve independent financial advisors and help them create great outcomes for their clients. Through a consistent program of engagement, education and community-building, AssetMark brings like-minded advisors together to share their ideas, experiences and solutions. This collaboration guides our development of investment, client relationship and practice management tools that advisors can tap to help clients achieve their investment objectives and life goals.We are always seeking bright, creative, energetic individuals who want to learn and grow with the company. AssetMark is an innovative organization, delivering the best services in the industry. We offer a rewarding work environment, competitive salaries, full benefits, and excellent opportunities for career growth. We are headquartered in the San Francisco Bay Area. We also have regional offices in Phoenix, AZ, Encino, CA, Atlanta, GA, Chicago, IL, and State College, PA. Become a part of our growing team today!"
    },

    { id: 8, role: "Business Consultant", jobid: "xx-xxx", datetime: "19 Mar 2019 | 05:35 pm", jobindustry: 'IT industry', companyname: "AssetMark", joblocation: "Chicago, IL", detailpera: "We are seeking a Business Consultant to lead our advisor consulting services across the Central region.  This critical role is responsible for driving and owning the execution of business consulting to help our Investment Advisors optimize their goals and take their practice to the next level.  Your passion for improving process, your experience in evaluating a plan of action and your ability to develop strategic solutions that enhance operations and boost business, are a great match for this position at AssetMark.  To be successful, this position will need a high degree of collaboration, creative thinking, exceptional problem-solving and project management skills, and a high attention to detail." ,
    principalResponsibilities:[
      "Responsibility for assessing, analyzing and advising clients on all aspects of business/practice management, particularly in the areas of business strategy and planning, human capital, operations, branding, marketing and business transition (M&A, valuation and succession planning). ",
      "Responsibility for managing client engagements in coordination with consulting team, including project management, relationship management, business analytics, and client advice.  Maintain excellent client relationships and deliver results, ensuring a positive client experience. Collaborate with the AssetMark Sales Team to develop and deepen advisor relationships in an effort to improve client loyalty, drive AUM growth and improve asset retention",
      "Design, direct and develop business strategies and solutions to meet client needs, including, but not limited to:",
      "Consulting, advising, and creating programs to fill the gaps between actual situations and desired ones",
      "Developing management and supervisory skills for advisors and staff",
      "Improving work performance and organizational communication",
      "Increasing employee motivation and morale",
      "Defining the company’s mission, goals and objectives",
      "Achieving customer satisfaction and customer loyalty",
      "Coaching, guiding and training employees",
      "Raising the company’s customer base and market segment",
      "Design, direct and draft client work product, review and approve work product from consulting team.",
      "Participate in the marketing and awareness campaigns for new consulting concepts, services and key business initiatives.",
      "Utilize Salesforce and other technology according to protocol to report, track and manage all client engagements, activities and opportunities. ",
      "Provide training, mentorship and education to Business Consulting team members as well as other staff of AssetMark to ensure understanding of function, services and support.Consultants will spend approximately 50% of their time traveling to client offices, speaking engagements and professional development seminars."
    ],
    requiredQualifications:[],
    preferredQualifications:[],
    requiredSkills:[
      "5-10 years of business consulting and/or practice management experience",
      "Bachelor’s Degree, with advanced degree or financial designations desirable",
      "Strong understanding of small business consulting principals, strategies and solutions",
      "Strong organizational skills and project management skills with the demonstrated ability to develop and implement practical strategies, plans, and to carry forward an idea or project from conception to execution",
      "Able to self-direct and manage time and multiple priorities on a day-to-day basis",
      "Track record of taking initiative and managing competing priorities",
      "Works effectively under pressure when facing extremely short deadlines",
      "Attention to detail is critical",
      "Excellent communication skills, written and verbal",
      "Able to think creatively, display intellectual curiosity and master complex subject matter quickly",
      "Superior critical thinking skills, including the capacity to identify and appropriately assess business challenges",
      "Exceptional research, writing, analytical and organizational skills, and the ability to convey complex concepts in a clear, concise and logical manner",
      "Partners well with colleagues and outside business partners to get work done",
      "A positive, team oriented, can-do attitude",
      "Must be able to work effectively in fast-paced, changing environment",
      "Advanced knowledge of Microsoft Office® (Word, Excel, PowerPoint, Outlook)"
    ],
    aboutCompany:"AssetMark, Inc. is a leading strategic provider of groundbreaking investment and consulting solutions created to serve independent financial advisors and help them create great outcomes for their clients. Through a consistent program of engagement, education and community-building, AssetMark brings like-minded advisors together to share their ideas, experiences and solutions. This collaboration guides our development of investment, client relationship and practice management tools that advisors can tap to help clients achieve their investment objectives and life goals.We are always seeking bright, creative, energetic individuals who want to learn and grow with the company. AssetMark is an innovative organization, delivering the best services in the industry. We offer a rewarding work environment, competitive salaries, full benefits, and excellent opportunities for career growth. We are headquartered in the San Francisco Bay Area. We also have regional offices in Phoenix, AZ, Encino, CA, Atlanta, GA, Chicago, IL, and State College, PA. Become a part of our growing team today!"
    },

    { id: 9, role: "Financial Services Relationship Manager", jobid: "xx-xxx", datetime: "19 Mar 2019 | 05:35 pm", jobindustry: 'IT industry', companyname: "AssetMark", joblocation: "Phoenix, AZ", detailpera: "The Advisor Services Relationship Manager provides support to Financial Advisors on new client and account set-up procedures and issues, in addition to account change request support and tracking. The Advisor Services Relationship Manager also provides administrative training and website usability support.We are hiring for a class starting April 15th. Get your applications in now for consideration! " ,
    principalResponsibilities:[
      "Provide telephone and email-based support to Financial Advisors that use the AssetMark platform, through training, communicating procedures, documentation requirements and account processing best practices",
      "Fields inquires through phone call and email from Advisors and their staff and addresses “How-to” questions, issues and concerns. Takes total ownership and accountability to all interactions with internal and external customers",
      "Initiates outbound communications with Advisors regarding open issues and concerns based on inquiries",
      "Must have strong oral and written presence by being positive, energetic, helpful, professional, clear and confident",
      "Reports all “outside/in” and “inside/out” defects to the Customer Experience Group and communicates ongoing progress toward resolution to the Advisor until the resolution is completed; provides feedback to the Advisor regarding changes in corporate policies/procedures or related areas which were implemented as a result of the identified deficit",
      "Continually evaluate day-to-day activities for process improvement opportunities; speaks about outages, offers solutions and makes improvements; Must be solution oriented",
      "Manage firm risk by setting appropriate expectations",
      "Supports and adheres to departmental standards for call management"
    ],
    requiredQualifications:[
      "Minimum 2+ years of experience in the brokerage industry/banking",
      "1+ year of data entry/customer service experience",
      "Bachelor’s degree or equivalent experience",
      "Minimum of Series 6 securities license"
    ],
    preferredQualifications:[
      "Series 6 or 7 license",
      "Proficiency in Microsoft Office",
      "Courteous and professional demeanor on the phone/web with customer interactions and internally with peers/other functions",
      "High energy, eager to learn, willing to cooperate",
      "Excellent interpersonal skills",
      "Ability to develop plans and follow through to a successful conclusion",
      "Ability to excel in a dynamic, fast-paced environment along with an ability to multi-task",
      "Excellent organization skills all while being self-motivated and having the ability to work independently and under little direction",
      "Able to get along with diverse personalities and comfort with being a “team player” and doing whatever is needed, big or small",
      "Consistently demonstrates a high degree of productivity",
      "Intermediate level working knowledge of investment products and services",
      "Has an ability to self-assess and has a passion for self-improvement",
      "Displays passion and a heart for service"
    ],
    requiredSkills:[],
    aboutCompany:"AssetMark, Inc. is a leading strategic provider of groundbreaking investment and consulting solutions created to serve independent financial advisors and help them create great outcomes for their clients. Through a consistent program of engagement, education and community-building, AssetMark brings like-minded advisors together to share their ideas, experiences and solutions. This collaboration guides our development of investment, client relationship and practice management tools that advisors can tap to help clients achieve their investment objectives and life goals.We are always seeking bright, creative, energetic individuals who want to learn and grow with the company. AssetMark is an innovative organization, delivering the best services in the industry. We offer a rewarding work environment, competitive salaries, full benefits, and excellent opportunities for career growth. We are headquartered in the San Francisco Bay Area. We also have regional offices in Phoenix, AZ, Encino, CA, Atlanta, GA, Chicago, IL, and State College, PA. Become a part of our growing team today!"
    },


    { id: 9, role: "Operations Case Manager (Financial Services)", jobid: "xx-xxx", datetime: "19 Mar 2019 | 05:35 pm", jobindustry: 'IT industry', companyname: "AssetMark", joblocation: "Phoenix, AZ", detailpera: "AssetMark is a leading strategic provider of innovative investment and consulting solutions serving independent financial advisors. We provide investment, relationship and practice management solutions that advisors use in helping clients achieve wealth, independence and purpose.A successful New Business Case Manager will bring a continuous improvement mindset to a growing asset management operation." ,
    principalResponsibilities:[
      "Process new business in a key team role supporting AssetMark's Referrer and Advisory distribution channel serving independent investment advisors and their clients",
      "Take individual ownership for delivery on cycle-time, accuracy, and compliance standards on all new business applications as part of a high-performance team of accountable, self-directed professionals",
      "Process new applications and accounts that are in good order (IGO) within cycle-time and accuracy standards",
      "Must satisfy advisors with timely, complete, knowledgeable and responsive communications regarding work that is not in good order (NIGO) assisting the advisors in resolving open issues",
      "Handle new accounts from Initial Work Review to account set-up and funding",
      "Must demonstrate proficiency quickly in all aspects of new business processing, new business requirements, and responsive advisor communications",
      "Need to be able to flex to perform various operational tasks as needed",
      "Must communicate effectively with functional peers and leaders in related functional areas, such as Quality, Investor Services, Transfers and Global Support processes in order to create an excellent customer service experience consistent with AssetMark values"
      
    ],
    requiredQualifications:[
      "2+ years’ of experience in the brokerage industry/banking",
      "1+ year of data entry experience",
      "1+ year of customer service experience"
    ],
    preferredQualifications:[
      "Series 6 or 7 license",
      "Energetic, eager to learn, willing to cooperate",
      "Superior written and verbal communication skills",
      "Excellent interpersonal skills",
      "Ability to develop plans and follow through to a successful conclusion",
      "Ability to excel in a dynamic, fast-paced environment",
      "Ability to multi-task",
      "Excellent organization skills",
      "Proficiency in Microsoft Office",
      "Comfort with being a “team player” and doing whatever is needed, big or small",
      "Able to get along with diverse personalities",
      "Self-motivated with ability to work independently and under direction",
      "Follow all firm processes, procedures, and protocols",
      "Courteous and professional demeanor on the phone/web with customer interactions and internally with peers/other functions",
      "Consistently demonstrates a high degree of productivity"
    ],
    requiredSkills:[],
    aboutCompany:"AssetMark, Inc. is a leading strategic provider of groundbreaking investment and consulting solutions created to serve independent financial advisors and help them create great outcomes for their clients. Through a consistent program of engagement, education and community-building, AssetMark brings like-minded advisors together to share their ideas, experiences and solutions. This collaboration guides our development of investment, client relationship and practice management tools that advisors can tap to help clients achieve their investment objectives and life goals.We are always seeking bright, creative, energetic individuals who want to learn and grow with the company. AssetMark is an innovative organization, delivering the best services in the industry. We offer a rewarding work environment, competitive salaries, full benefits, and excellent opportunities for career growth. We are headquartered in the San Francisco Bay Area. We also have regional offices in Phoenix, AZ, Encino, CA, Atlanta, GA, Chicago, IL, and State College, PA. Become a part of our growing team today!"
    }
  ];
   selectedJobDetails = this.data2[1];

  page = 0;
  size = 6;
  detailsOption=-1;
  skillsAskpiedata;
  skillsMatchpiedata;
  skillsIntellimatchpiedata;
  mScoreMatchpiedata;
  ngOnInit() {
    this.skillsAskpiedata = {
      labels: ["Management", "Analysis","Data Scince"],
      datasets: [
        {
          data: [10, 25, 65],
          backgroundColor: ["#FF6384", "#36A2EB","#bbb"],
          hoverBackgroundColor: ["#FF6384", "#36A2EB","#ccc"]
        }
      ]
    };
    this.skillsMatchpiedata = {
      labels: ["Management", "Analysis","Data Scince"],
      datasets: [
        {
          data: [10, 25, 65],
          backgroundColor: ["#FF6384", "#36A2EB","#bbb"],
          hoverBackgroundColor: ["#FF6384", "#36A2EB","#ccc"]
        }
      ]
    };
    this.skillsIntellimatchpiedata = {
      labels: ["Management", "Analysis","Data Scince"],
      datasets: [
        {
          data: [10, 25, 65],
          backgroundColor: ["#FF6384", "#36A2EB","#bbb"],
          hoverBackgroundColor: ["#FF6384", "#36A2EB","#ccc"]
        }
      ]
    };
    this.mScoreMatchpiedata = {
      labels: ["Management", "Analysis","Data Scince"],
      datasets: [
        {
          data: [10, 25, 65],
          backgroundColor: ["#FF6384", "#36A2EB","#bbb"],
          hoverBackgroundColor: ["#FF6384", "#36A2EB","#ccc"]
        }
      ]
    };

    this.getData({ pageIndex: this.page, pageSize: this.size });
  }
  
  getData(obj) {
    console.log('in get data');
    window.scrollTo(0, 0);
    let index = 0,
      startingIndex = obj.pageIndex * obj.pageSize,
      endingIndex = startingIndex + obj.pageSize;

    this.data = this.data2.filter(() => {
      index++;
      document.querySelector('#top').scrollTop = 1500;  ;
      
      return (index > startingIndex && index <= endingIndex) ? true : false;

    });
  }

  setId(id){
    this.selectedJobDetails=this.data2[id-1]
  }

  searchFilter(){
    this.data=[]
    this.data.push(this.data2[0]);
  }
  filterChange(){
    if(this.detailsOption == 2){
      // this.yourSkills1 = this.yourSkills
      // this.previousRoles1 = this.previousRoles
      this.yourSkills1 = this.yourSkills.filter(el =>{return el.color == "#336688" || el.color == "#bde4ff"})
      this.previousRoles1 = this.previousRoles.filter(el => {return el.color == "#336688" || el.color == "#bde4ff"})
    }else{
      this.yourSkills1 = this.yourSkills.filter(el =>{return el.color == "#bde4ff"})
      this.previousRoles1 = this.previousRoles.filter(el => {return el.color == "#bde4ff"})
    }
  }
  clearFilter(){
    this.detailsOption = -1;
     this.yourSkills1 = this.yourSkills
      this.previousRoles1 = this.previousRoles
  }
  applyForJobfn(){
    this.applied=true;
  }
}


