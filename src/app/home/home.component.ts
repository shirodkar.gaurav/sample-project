import { Component, OnInit,AfterViewInit } from '@angular/core';
// import {ReactiveFormsModule,  FormBuilder, FormGroup, FormControl, Validators,AbstractControl} from '@angular/forms';
import { ReactiveFormsModule, ValidationErrors, FormGroup, FormControl, Validators, FormBuilder, EmailValidator, ValidatorFn, AbstractControl } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { ActivatedRoute, Route } from '@angular/router';


// import { Http, Headers, RequestOptions } from '@angular/http';
declare var $: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  contactform: FormGroup;
  subscribeemailform: FormGroup;
  contact_form_name:String;
  contact_form_email:String;
  contact_form_message:String;
  email_address:String;
  error_message:String;
  success_message:String;
  // dataLoading:Boolean;
  // public bsModal:String="hello";
  homeHeaderData = { 'homelink': true,'aboutuslink': true,'careerlink': true,'joblink': true,'companylink': true,'contactlink': true };
  constructor(private http: HttpClient,private router: Router,private route: ActivatedRoute ) {
   }

  ngOnInit() {
    this.createForm()
  }

  ngAfterViewInit() 
  {
    $('.navigation .nav a[href^="#"], a[href^="#"].roll').on('click',function (e) {
      e.preventDefault();
      console.log("bye");
      var target = this.hash,
          $target = $(target);
      if (document.documentElement.clientWidth > 768) {
          $('html, body').stop().animate({
              'scrollTop': $target.offset().top - $('.navigation').height()
          }, 2000)
      } else {
          $('html, body').stop().animate({
              'scrollTop': $target.offset().top
          }, 2000)
      }
  });
      $('.parallax-window').parallax({imageSrc: 'assets/images/slide-01.jpg'}); 
        function myFunction() {
               var element = document.getElementById("nav-scroll");
                element.classList.remove("in"); 
        console.log("hello")
        }
 }

  createForm() {
    this.contactform = new FormGroup({
      "contact_form_name": new FormControl("", Validators.compose([
        Validators.required,
      ])),
      "contact_form_email": new FormControl("", Validators.compose([
        Validators.required,
        this.emailDomainValidator,
      ])),
      "contact_form_message": new FormControl("", Validators.compose([
        Validators.required,
      ])),
    });
    this.subscribeemailform = new FormGroup({
      "email_address": new FormControl("", Validators.compose([
        Validators.required,
        this.emailDomainValidator,
      ])),
    });
  }
  
  displaymodal()
  {
    this.ngOnInit()
   
   }
    contactfunction()
   {
    this.error_message = "";
    this.success_message = "";
    // this.dataLoading = true;
    if (this.contactform.valid) {
      this.http.post('/mail/contact', this.contactform.value, {
        withCredentials: true,
        headers: new HttpHeaders().append('Content-Type', 'application/json')
      })
        .subscribe(res => {
            if (res) {
              // this.contactform.reset();
              // this.contactform.setValue({contact_form_message:''});
              this.success_message = 'Your Email was send!.';
              this.ngOnInit()
             setTimeout(() => {
               this.success_message=''
                this.router.navigate([''], { relativeTo: this.route });
              // this.dataLoading = true;
             }, 1500);
           }
           else {
            //  this.dataLoading = false;
              this.error_message = "Something went wrong.";
           }
         });
      } else {
      // this.error_message = "Profile Not Updated";
      this.validateAllFormFields(this.contactform);
      // this.dataLoading = false;
    }
   }

    subscribeemailfunction(){
      // this.dataLoading = true;
      if (this.subscribeemailform.valid) {
        console.log(this.subscribeemailform.value)
        this.http.post('/mail/subscribe', this.subscribeemailform.value, {
          withCredentials: true,
          headers: new HttpHeaders().append('Content-Type', 'application/json')
        })
          .subscribe(res => {
            if (res) {
                 this.success_message = 'Thank you for signing up.';
                setTimeout(() => {
                  this.success_message=''
                  $('#messagepopup').modal('hide');
                  this.router.navigate([''], { relativeTo: this.route });
                  this.subscribeemailform.reset();
                  // this.dataLoading = true;
                }, 1500);
              }
              else {
                // this.dataLoading = false;
                 this.error_message = "Something went wrong.";
              }
            });
         } else {
        this.validateAllFormFields(this.subscribeemailform);
      } 
    }
validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

emailDomainValidator(control: AbstractControl): { [key: string]: boolean } | null {
    if (control.value != null || control.value.length != 0) {
      let email = control.value;
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase()) ? null : { 'compare': true };
    }
    return null;
  }

}
