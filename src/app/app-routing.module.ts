import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';  
import { CareersComponent } from './careers/careers.component'; 
import { JobsComponent } from './jobs/jobs.component'; 
import { CompanyJobsComponent } from './company-jobs/company-jobs.component'; 
import { ContactComponent } from './contact/contact.component'; 

const routes: Routes = [ 
  { path: '', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'careers', component: CareersComponent}, 
  { path: 'jobs', component: JobsComponent },
  { path: 'company-jobs', component: CompanyJobsComponent },
  { path: 'contact', component: ContactComponent } 

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]  
  
})
export class AppRoutingModule { }
export  const  routingComponents = [HomeComponent, AboutComponent, CareersComponent ,JobsComponent,  CompanyJobsComponent, ContactComponent];

 
