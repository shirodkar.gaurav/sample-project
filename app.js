var express = require('express');
var path= require('path')
var bodyParser = require('body-parser');
var http = require('http');
var app = express();
var mail = require('./routes/mail');
// const router = express.Router();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(express.static(path.join(__dirname, 'dist/ng5')));

app.use('/mail', mail);
app.use('/static', express.static(path.join(__dirname + '/public')));
app.get('*', function (req, res, next) {
    res.sendFile(__dirname + "/dist/ng5/index.html");
});

const port = process.env.PORT || '3007';
app.set('port', port);

const server = http.createServer(app);


server.listen(port, function () {
    console.log('Server listening at port ', port);
});

